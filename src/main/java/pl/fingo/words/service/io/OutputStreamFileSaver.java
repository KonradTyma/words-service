package pl.fingo.words.service.io;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import lombok.extern.slf4j.Slf4j;

@SuppressWarnings("SameParameterValue")
@Slf4j
public class OutputStreamFileSaver {

    private static final int DEFAULT_BUFFER_SIZE = 8192;

    public boolean writeBufferedContent(String filename, List<String> txtContent) {
        try (FileWriter writer = new FileWriter(filename)) {
            BufferedWriter bufferedWriter = new BufferedWriter(writer, DEFAULT_BUFFER_SIZE);
            for (String line : txtContent) {
                bufferedWriter.write(line);
                bufferedWriter.write(System.lineSeparator());
            }

        } catch (IOException e) {
            log.warn("IO Exception occurred, result not save into file");
        }
        return true;
    }

    public boolean writeContent(String filename, List<String> txtContent) {
        try (FileOutputStream outputStream = new FileOutputStream(filename)) {
            for (String line : txtContent) {
                outputStream.write(line.getBytes());
                outputStream.write(System.lineSeparator().getBytes());
            }

        } catch (IOException e) {
            log.warn("IO Exception occurred, result not save into file");
        }
        return true;
    }

}