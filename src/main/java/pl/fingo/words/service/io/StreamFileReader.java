package pl.fingo.words.service.io;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import lombok.extern.slf4j.Slf4j;

@SuppressWarnings("SameParameterValue")
@Slf4j
public class StreamFileReader {

    public List<String> readFromFile(String fileToRead) {
        List<String> txtContent = new ArrayList<>();
        try (BufferedReader bufferedReader = Files.newBufferedReader(Paths.get(fileToRead), StandardCharsets.UTF_8)) {
            txtContent = bufferedReader.lines().collect(Collectors.toList());
        } catch (IOException ex) {
            log.warn("IO Exception occurred, can not read from file: {}", fileToRead);
        }
        return txtContent;
    }

}
