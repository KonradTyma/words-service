package pl.fingo.words.service.analyzer;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ForkJoinPool;
import java.util.stream.Collectors;

class WordsAnalyzerService {

    private static final String DELIMITER = "";
    private static final int POOL_SIZE = 8;

    Map<String, Long> analyzeContent(Collection<String> wordsOfContent) throws ExecutionException, InterruptedException {
        if (wordsOfContent.isEmpty()) {
            return Collections.emptyMap();
        }
        ForkJoinPool forkJoinPool = new ForkJoinPool(POOL_SIZE);
        String contentAsString = String.join(DELIMITER, wordsOfContent);

        Map<String, Long> countedGroupedLetter = forkJoinPool.submit(parallelCountGroupedLetter(contentAsString)).get();
        return new TreeMap<>(countedGroupedLetter);
    }

    private Callable<Map<String, Long>> parallelCountGroupedLetter(String contentAsString) {
        return () -> Arrays.stream(contentAsString.split(DELIMITER))
                .parallel()
                .collect(Collectors.groupingBy(letter -> letter, Collectors.counting()));
    }

    private Map<String, Long> countGroupedLetter(String contentAsString) {
        return Arrays.stream(contentAsString.split(DELIMITER)).collect(Collectors.groupingBy(letter -> letter, Collectors.counting()));
    }

}

