package pl.fingo.words.service.analyzer;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

class ResultFormatter {

    private static final String KEY_VALUE_FORMAT = "%s : %d";

    static List<String> formatFrom(Map<String, Long> analyzedContent) {
        return analyzedContent.entrySet().stream()
                .map(entry -> String.format(KEY_VALUE_FORMAT, entry.getKey(), entry.getValue()))
                .collect(Collectors.toList());
    }

}
