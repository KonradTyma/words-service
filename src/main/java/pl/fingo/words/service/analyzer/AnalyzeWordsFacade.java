package pl.fingo.words.service.analyzer;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import lombok.extern.slf4j.Slf4j;
import pl.fingo.words.service.io.OutputStreamFileSaver;
import pl.fingo.words.service.io.StreamFileReader;

@Slf4j
public class AnalyzeWordsFacade {

    private static final String FILE_TO_READ = "generated_words_data.txt";
    private static final String FILE_TO_SAVE = "analyzed_words_data.txt";

    private final WordsAnalyzerService wordsAnalyzer;
    private final StreamFileReader streamFileReader;
    private final OutputStreamFileSaver streamFileSaver;

    public AnalyzeWordsFacade() {
        this.streamFileSaver = new OutputStreamFileSaver();
        this.wordsAnalyzer = new WordsAnalyzerService();
        this.streamFileReader = new StreamFileReader();
    }

    public void saveGroupedWordsLetters() {
        List<String> wordsOfContent = streamFileReader.readFromFile(FILE_TO_READ);
        saveAnalyzedContent(wordsOfContent);
    }

    public void saveGroupedUniqueWordsLetters() {
        List<String> wordsOfContent = streamFileReader.readFromFile(FILE_TO_READ);
        HashSet<String> uniqueWords = new HashSet<>(wordsOfContent);
        saveAnalyzedContent(uniqueWords);
    }

    private void saveAnalyzedContent(Collection<String> content) {
        try {
            Map<String, Long> analyzedContent = wordsAnalyzer.analyzeContent(content);
            streamFileSaver.writeContent(FILE_TO_SAVE, ResultFormatter.formatFrom(analyzedContent));
        } catch (ExecutionException | InterruptedException ex) {
            log.warn("Operation interrupted! ", ex);
        }
    }

}
