package pl.fingo.words.service.generator;

import java.util.List;

import pl.fingo.words.service.io.OutputStreamFileSaver;

public class GeneratorWordsFacade {

    private static final String FILENAME = "generated_words_data.txt";
    private static final int NUMBER_OF_WORDS = 4000000;

    private final WordsGeneratorService generatorService;
    private final OutputStreamFileSaver fileSaver;

    public GeneratorWordsFacade() {
        this.generatorService = new WordsGeneratorService();
        this.fileSaver = new OutputStreamFileSaver();
    }

    public void saveGeneratedContentToFile() {
        List<String> content = generatorService.generateTxtContent(NUMBER_OF_WORDS);
        fileSaver.writeBufferedContent(FILENAME, content);
    }

}
