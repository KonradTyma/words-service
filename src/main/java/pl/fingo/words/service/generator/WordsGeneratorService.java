package pl.fingo.words.service.generator;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@SuppressWarnings("SameParameterValue")
class WordsGeneratorService {

    private static final String UPPER_LETTERS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    private static final WordLengthRange DEFAULT_WORD_RANGE = WordLengthRange.of(2, 10);

    private final Random random = new Random();

    List<String> generateTxtContent(int numberOfWords) {
        return IntStream.range(0, numberOfWords)
                .mapToObj(word -> generateWord(DEFAULT_WORD_RANGE))
                .collect(Collectors.toCollection(() -> new ArrayList<>(numberOfWords)));
    }

    private String generateWord(WordLengthRange wordLengthRange) {
        int wordLength = getRandomWordLength(wordLengthRange);
        return IntStream.range(0, wordLength)
                .mapToObj(word -> String.valueOf(UPPER_LETTERS.charAt(random.nextInt(UPPER_LETTERS.length()))))
                .collect(Collectors.joining());
    }

    private int getRandomWordLength(WordLengthRange wordLengthRange) {
        return random.nextInt(wordLengthRange.getMaxLength()) + wordLengthRange.getMinLength();
    }

}
