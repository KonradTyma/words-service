package pl.fingo.words.service.generator;

import lombok.Value;

@Value(staticConstructor = "of")
class WordLengthRange {

    Integer minLength;
    Integer maxLength;
}
