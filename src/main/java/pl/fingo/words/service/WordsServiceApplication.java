package pl.fingo.words.service;

import pl.fingo.words.service.analyzer.AnalyzeWordsFacade;
import pl.fingo.words.service.generator.GeneratorWordsFacade;

public class WordsServiceApplication {

    private static final GeneratorWordsFacade GENERATOR_WORDS_FACADE = new GeneratorWordsFacade();
    private static final AnalyzeWordsFacade ANALYZER_WORDS_FACADE = new AnalyzeWordsFacade();

    public static void main(String[] args) {
        stepOne();
        stepTwo();
        stepThree();
    }

    private static void stepOne() {
        GENERATOR_WORDS_FACADE.saveGeneratedContentToFile();
    }

    private static void stepTwo() {
        ANALYZER_WORDS_FACADE.saveGroupedWordsLetters();
    }

    private static void stepThree() {
        ANALYZER_WORDS_FACADE.saveGroupedUniqueWordsLetters();
    }

}
