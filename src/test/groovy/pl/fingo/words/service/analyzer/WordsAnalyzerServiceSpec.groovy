package groovy.pl.fingo.words.service.analyzer

import pl.fingo.words.service.analyzer.WordsAnalyzerService
import spock.lang.Specification

@SuppressWarnings("GroovyAccessibility")
class WordsAnalyzerServiceSpec extends Specification {

    def analyzer = new WordsAnalyzerService()

    def 'should sort, group and count letters in given txt content'() {
        when:
        def result = analyzer.analyzeContent(txtContent)

        then:
        result
        result instanceof TreeMap
        result.toString() == expectedResult.toString()

        where:
        txtContent                                || expectedResult
        ['ALA', 'MA', 'KOTA', 'KOT', 'MA', 'ALE'] || [A: 6, E: 1, K: 2, L: 2, M: 2, O: 2, T: 2]
    }

    def 'should return empty map for empty txt content '() {
        when:
        def result = analyzer.analyzeContent([])

        then:
        noExceptionThrown()
        result == [:]
    }

}
