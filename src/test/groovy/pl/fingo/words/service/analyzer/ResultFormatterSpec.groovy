package groovy.pl.fingo.words.service.analyzer

import pl.fingo.words.service.analyzer.ResultFormatter
import spock.lang.Specification

class ResultFormatterSpec extends Specification {

    def 'should format map to key value rows'() {
        given:
        def contentAsMap = ['A': 1, 'B': 5, 'C': 13]
        def expectedData = ['A : 1', 'B : 5', 'C : 13']

        when:
        def formattedData = ResultFormatter.formatFrom(contentAsMap)

        then:
        formattedData
        formattedData.size() == contentAsMap.size()
        formattedData == expectedData
    }
}
