package groovy.pl.fingo.words.service.generator

import pl.fingo.words.service.generator.WordLengthRange
import pl.fingo.words.service.generator.WordsGeneratorService
import spock.lang.Specification
import spock.lang.Unroll

@SuppressWarnings(value = ["GroovyAssignabilityCheck", "GroovyAccessibility"])
class WordsGeneratorServiceSpec extends Specification {

    def service = new WordsGeneratorService()

    @Unroll
    def 'should generate list of words by given number of words : #numberOfWords'() {
        when:
        def content = service.generateTxtContent(numberOfWords)

        then:
        content
        content.size() == numberOfWords

        where:
        numberOfWords << [10, 100, 1000, 10000, 100000, 1000000]
    }

    @Unroll
    def 'should generate random word with given length: #lengthRange from [A-Z]'() {
        when:
        def generatedWord = service.generateWord(lengthRange)

        then:
        generatedWord

        where:
        lengthRange << [WordLengthRange.of(2, 10), WordLengthRange.of(5, 10), WordLengthRange.of(1, 1)]
    }

    @Unroll
    def 'should throw exception of illegal argument when word range is invalid. #description'() {
        when:
        def generatedWord = service.generateWord(lengthRange)

        then:
        !generatedWord
        thrown(IllegalArgumentException)

        where:
        lengthRange              || description
        WordLengthRange.of(0, 0) || 'Bound must be positive'
    }

}
