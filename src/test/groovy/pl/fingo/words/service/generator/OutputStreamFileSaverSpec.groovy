package groovy.pl.fingo.words.service.generator

import org.apache.commons.lang3.RandomStringUtils
import pl.fingo.words.service.io.OutputStreamFileSaver
import spock.lang.Specification
import spock.lang.Unroll

@SuppressWarnings(value = ["GroovyAssignabilityCheck", "GroovyAccessibility"])
class OutputStreamFileSaverSpec extends Specification {

    def outputStreamFileSaver = new OutputStreamFileSaver()

    @Unroll
    def 'should save content of #numberOfWords words to file'() {
        given:
        def filename = 'example_data.txt'
        def content = generateContent(numberOfWords)

        when:
        def result = outputStreamFileSaver.writeBufferedContent(filename, content)

        then:
        result

        where:
        numberOfWords << [100, 1000, 10000, 100000, 1000000]
    }

    def generateContent(numberOfWords) {
        List<String> content = []
        numberOfWords.times {
            content.add(RandomStringUtils.randomAlphabetic(10))
        }
        content
    }
}
